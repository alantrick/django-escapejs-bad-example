import django.shortcuts
import django.http


def home(request):
    return django.shortcuts.render(
        request, 'django_escapejs_bad_example/home.html', {
            'name': request.POST.get('name', '')
        }
    )
