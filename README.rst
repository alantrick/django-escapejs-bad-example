=====================================================
 Sample XSS Exploit From Unvalidated Use of escapejs
=====================================================

.. warning:: Do not under any cirumstances use this code. It contains
    a security vulnerability. It only exists as a warning to those
    who might want to ``escapejs`` without understanding the context.


In order to see how to abuse ``escapejs``, you can download this
and run ``./manage.py runserver`` and then go to ``http://127.0.0.1/``.